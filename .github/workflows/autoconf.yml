#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
#
# autoconf.yml -- Configuration for autoconf GitHub Action workflow.
#

name: autoconf

on:
  push:
    branches: ["*"]
  pull_request:
    branches: ["*"]

concurrency:
  group: ${{ github.workflow }}-${{ github.ref }}
  cancel-in-progress: false

jobs:
  build:
    strategy:
      fail-fast: false
      matrix:
        check-target: [check, davautocheck]
        os: [ubuntu-latest, ubuntu-22.04-arm]

    runs-on: ${{ matrix.os }}
    name: ${{ matrix.os }}, target ${{ matrix.check-target }}

    steps:
      - name: Install dependencies (Linux, apt-get)
        if: runner.os == 'Linux'
        run: >
          sudo apt-get update &&
          sudo apt-get install
          libtool
          libtool-bin
          libapr1-dev
          libaprutil1-dev
          libserf-dev
          libexpat1-dev
          zlib1g-dev
          libsqlite3-dev
          liblz4-dev
          libutf8proc-dev
          apache2-dev
          libsecret-1-dev

      - name: Use LF for Git checkout
        run: |
          git config --global core.autocrlf false
          git config --global core.eol lf

      - uses: actions/checkout@v4

      - name: autogen
        run: ./autogen.sh

      - name: Configure
        run: ./configure --enable-maintainer-mode

      - name: Build (make)
        run: make -j

      - name: Run tests
        run: make ${{matrix.check-target}} PARALLEL=16 APACHE_MPM=event

      - name: Archive test log
        if: always()
        uses: actions/upload-artifact@v4
        with:
          name: tests-${{matrix.os}}-${{matrix.check-target}}.log
          path: tests.log

      - name: Install (make install)
        run: sudo make install
